package com.shubhesh.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.shubhesh.client.shubheshService;

public class shubheshServiceImpl extends RemoteServiceServlet implements shubheshService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }
}