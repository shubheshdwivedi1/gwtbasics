package com.shubhesh.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class uiBinder extends Composite {
    interface uiBinderUiBinder extends UiBinder<Widget, uiBinder> {}
    private static uiBinderUiBinder ourUiBinder = GWT.create(uiBinderUiBinder.class);

    public uiBinder() {
         initWidget(ourUiBinder.createAndBindUi(this));
    }
}