package com.shubhesh.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("shubheshService")
public interface shubheshService extends RemoteService {
    // Sample interface method of remote interface
    String getMessage(String msg);

    /**
     * Utility/Convenience class.
     * Use shubheshService.App.getInstance() to access static instance of shubheshServiceAsync
     */
    public static class App {
        private static shubheshServiceAsync ourInstance = GWT.create(shubheshService.class);

        public static synchronized shubheshServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
