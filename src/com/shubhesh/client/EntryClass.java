package com.shubhesh.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.*;

public class EntryClass implements EntryPoint {

    public void onModuleLoad() {
        RootPanel.get().add(new uiBinder());
    }

}
